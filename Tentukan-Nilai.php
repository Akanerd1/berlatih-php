<?php
  echo "<h1>Tentukan Nilai</h1>";
function tentukan_nilai($number)
{
    switch ($number) {
        case "98":
          echo  "Nilai : $number - Sangat Baik <br>";
          break;
        case "76":
            echo "Nilai : $number - Baik <br>";
          break;
        case "67":
            echo "Nilai : $number - Cukup <br>";
          break;
          case "43":
            echo "Nilai : $number - Kurang <br>";
          break;
        default:
          echo "Nilai Tidak ada dalam kategori! <br>";
      }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
