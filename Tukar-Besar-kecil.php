<?php
function tukar_besar_kecil($string)
{
    //kode di sini
    $hasil ="";
    for($i= 0;$i<strlen($string);$i++){
        $str = $string[$i];
        if (ctype_alpha($str) && $str === strtolower($str)) {
            $hasil .= strtoupper($str);
        } else if (ctype_alpha($str) && $str === strtoupper($str)) {
            $hasil .= strtolower($str);
        } else {
            $hasil .= $string[$i];
        };
    };
    return $hasil ;
};


// TEST CASES
echo tukar_besar_kecil('Hello World');         echo "<br>"; // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY');           echo "<br>"; // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!');   echo "<br>";// "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me');     echo "<br>"; // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW');      echo "<br>"; // "001-a-3-5tRDyw"
